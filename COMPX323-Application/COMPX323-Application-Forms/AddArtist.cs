﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COMPX323_Application_Forms
{
    public partial class AddArtist : Form
    {
        Boolean song_Add = true;
        Boolean artist_Add = false;
        Boolean is_oracle = true;
        public AddArtist(bool is_oracle)
        {
            InitializeComponent();
            comboBox1.Text = "Unspecified";
            this.is_oracle = is_oracle;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            //Song song = new Song(title, length, originalReleaseDate, lyrics, artistIDs, null);

            string stringId = "";
            long Id = 0;
            string fName;
            string lName;
            string phoneNum;
            string email;
            Person.Genders? gender;

            fName = fnametxt.Text;

            lName = lnametxt.Text;

            phoneNum = phnumtxt.Text;

            email = emailtxt.Text;

            if (comboBox1.SelectedText == "Unspecified")
            {
                gender = null;
            }
            else
            {
                gender = (Person.Genders)Enum.Parse(typeof(Person.Genders), comboBox1.Text.ToLower());
            }
            bool worked = false;

            if (is_oracle)
            {
                Person person = new Person(0, fName, lName, phoneNum, email, gender);
                long? id = OracleAccess.AddPerson(person);
                if (!id.HasValue)
                {
                    goto outpoint;
                }
                worked = OracleAccess.SetPersonIsArtist(id.Value, true);
            }
            else
            {
                Person person = new Person(null, fName, lName, phoneNum, email, gender);
                MongoAccess.AddPerson(person);
                worked = true;
            }
        outpoint:
            if (worked)
            {
                textbox_clear();
                MessageBox.Show("The artist has been added to database");
                this.Close();
            }
            else
            {
                MessageBox.Show("Rejected Unlucky");
            }

        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            textbox_clear();
        }

        private void textbox_clear()
        {
            fnametxt.Clear();
            lnametxt.Clear();
            emailtxt.Clear();
            phnumtxt.Clear();
        }
    }
}
