﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COMPX323_Application_Forms
{
    public partial class AddSong : Form
    {
        Boolean song_Add = true;
        Boolean artist_Add = false;
        Boolean is_oracle = true;
        public AddSong(bool is_oracle)
        {
            InitializeComponent();
            this.is_oracle = is_oracle;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            string title = "";
            string stringLength = "";
            int parsedValue;
            long length = 0;
            string date = "";
            DateTime originalReleaseDate;
            string lyrics = "";

            int? albumId;

            title = textBox1.Text;

            stringLength = textBox2.Text;
            if (!int.TryParse(textBox2.Text, out parsedValue))
            {
                MessageBox.Show("You must enter number for length");
                return;
            }
            else
            {
                length = Convert.ToInt64(stringLength);
            }

            //date = textBox3.Text;
            originalReleaseDate = dateTimePicker1.Value;
            bool worked = false;
            lyrics = textBox4.Text;
            if (textBox5.Tag != null)
            {
                if (is_oracle)
                {
                    List<long> artistIDs = new List<long>();
                    artistIDs.Add(((ArtistPreview)(textBox5.Tag)).Id);
                    Song song = new Song(title, length, originalReleaseDate, lyrics, artistIDs, null);
                    worked = OracleAccess.AddSong(song);
                }
                else
                {
                    var artistIDs = new List<BsonObjectId>();
                    artistIDs.Add(((ArtistPreview)(textBox5.Tag)).MongoId);
                    Song song = new Song(null, title, length, originalReleaseDate, lyrics, artistIDs, null);
                    worked = MongoAccess.AddSong(song);
                }

            }

            //Song song = new Song(title, length, originalReleaseDate, lyrics, artistIDs, null);

            if (worked)
            {
                textbox_clear();
                MessageBox.Show("The song has been added to database");
                this.Close();
            }
            else
            {
                MessageBox.Show("Rejected Unlucky");
            }

        }
        private void buttonClear_Click(object sender, EventArgs e)
        {
            textbox_clear();
        }

        private void textbox_clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            //textBox3.Clear();
            textBox4.Clear();
            textBox5.Clear();
            textBox6.Clear();
        }
        private void AddSongThing()
        {
            label1.Text = "Song Title :";
            label2.Text = "Song Length :";
            label3.Text = "Release Date :";
            label4.Text = "Lyrics :";
            label5.Text = "Artist Id :";
            label6.Text = "Album Id :";
        }
        private void AddArtist()
        {
            label1.Text = "Artist ID :";
            label2.Text = "First name :";
            label3.Text = "Last name :";
            label4.Text = "Phone Num :";
            label5.Text = "Email Acc :";
            label6.Text = "Gender(M/F) :";
        }

        private void addArtist_btn_Click(object sender, EventArgs e)
        {
            AddArtistToSong art = new AddArtistToSong(is_oracle);
            var res = art.ShowDialog();
            if (res == DialogResult.OK)
            {
                //Person? personInfo;
                //if(is_oracle == true)
                //{
                //    personInfo = OracleAccess.GetAllPersonInfo(art.ReturnVal);
                //}
                //else
                //{
                //    personInfo = MongoAccess.GetAllPersonInfo(art.ReturnVal);
                //}

                //if (personInfo.HasValue)
                //{
                textBox5.Tag = art.ReturnVal;
                textBox5.Text = art.ReturnVal.FirstName + " " + art.ReturnVal.LastName;
                /*
                detailsPanel.SetRow(detailsPanel.GetControlFromPosition(1, 4 + currentEditingSong.ArtistIDs.Count), 4 + currentEditingSong.ArtistIDs.Count + 1);
                var currentVal = labelTextbox(personInfo.Value.FirstName + " " + personInfo.Value.LastName, true);
                currentVal.Click += artistNameClick;
                currentVal.Tag = art.ReturnVal;
                detailsPanel.Controls.Add(currentVal, 1, 4 + currentEditingSong.ArtistIDs.Count);
                currentEditingSong.ArtistIDs.Add(art.ReturnVal);*/
                //}
            }
            else if (res == DialogResult.Cancel)
            {
                return;
            }
        }
    }
}
