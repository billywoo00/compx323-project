﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COMPX323_Application_Forms
{

    public partial class AddArtistToSong : Form
    {
        Boolean is_oracle;
        public ArtistPreview ReturnVal { get; set; }
        //public BsonObjectId ReturnValMongo { get; set; }

        public AddArtistToSong(bool is_oracle)
        {
            InitializeComponent();
            this.is_oracle = is_oracle;
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void AddArtist_Click(object sender, EventArgs e)
        {
            //if (is_oracle)
            //{
                this.ReturnVal = ((ArtistPreview)listBoxResult.SelectedItem);
            //} else
            //{
            //    this.ReturnValMongo = ((ArtistPreview)listBoxResult.SelectedItem).MongoId;
            //}

            this.DialogResult = DialogResult.OK;
        }

        private void Search_Click(object sender, EventArgs e)
        {
            listBoxResult.Items.Clear();
            List<ArtistPreview> allArtists;
            if(is_oracle == true)
            {
                allArtists = OracleAccess.GetArtistContaining(textBoxSearch.Text);
            }
            else
            {
                allArtists = MongoAccess.GetArtistContaining(textBoxSearch.Text);
            }
            
            foreach (var artist in allArtists)
            {
                listBoxResult.Items.Add(artist);
            }
        }
    }
}
