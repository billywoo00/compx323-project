﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using MongoDB.Bson;
namespace COMPX323_Application_Forms
{
    public struct ArtistPreview
    {
        public string FirstName { get; }
        public string LastName { get; }
        public long Id { get; }
        public BsonObjectId MongoId { get; }


        public ArtistPreview(long id, string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
            Id = id;
            MongoId = null;
        }

        public ArtistPreview(BsonObjectId id, string firstName, string lastName)
        {
            Id = 0;
            FirstName = firstName;
            LastName = lastName;
            MongoId = id;
        }

        public override string ToString()
        {
            return this.FirstName + " " + this.LastName;
        }
    }

    public struct SongId
    {
        public SongId(string title, DateTime releaseDate)
        {
            Id = null;
            Title = title;
            ReleaseDate = releaseDate;
        }

        public SongId(BsonObjectId id,string title, DateTime releaseDate)
        {
            Id = id;
            Title = title;
            ReleaseDate = releaseDate;
        }

        public BsonObjectId Id { get; }

        public string Title { get; }
        public DateTime ReleaseDate { get; }

        public override string ToString()
        {
            return this.Title + " " + this.ReleaseDate.ToShortDateString();
        }
    }

    public struct Song
    {
        public Song(string title, long length, DateTime originalReleaseDate, string lyrics, List<long> artistIDs, int? albumId)
        {
            Id = null;
            Title = title;
            Length = length;
            OriginalReleaseDate = originalReleaseDate;
            Lyrics = lyrics;
            ArtistIDs = artistIDs;
            AlbumID = albumId;
            ArtistIDsMongo = null;
        }

        public Song(BsonObjectId id,string title, long length, DateTime originalReleaseDate, string lyrics, List<BsonObjectId> artistIDs, int? albumId)
        {
            Id = id;
            Title = title;
            Length = length;
            OriginalReleaseDate = originalReleaseDate;
            Lyrics = lyrics;
            ArtistIDs = null;
            AlbumID = albumId;
            ArtistIDsMongo = artistIDs;
        }

        public BsonObjectId Id { get; set; }
        public string Title { get; set; }
        public long Length { get; set; }
        public DateTime OriginalReleaseDate { get; set; }
        public string Lyrics { get; set; }

        public List<long> ArtistIDs { get; set; }
        public List<BsonObjectId> ArtistIDsMongo { get; set; }
        public long? AlbumID { get; set; }
    }

    public struct Person
    {
        public enum Genders
        {
            male,
            female,
            other,
        }

        public Person(long id, string firstName, string lastName, string phoneNum, string email, Genders? gender)
        {
            this.id = id;
            FirstName = firstName;
            LastName = lastName;
            PhoneNum = phoneNum;
            Email = email;
            Gender = gender;
            mongoId = null;
        }

        public Person(BsonObjectId id, string firstName, string lastName, string phoneNum, string email, Genders? gender)
        {
            this.id = 0;
            mongoId = id;
            FirstName = firstName;
            LastName = lastName;
            PhoneNum = phoneNum;
            Email = email;
            Gender = gender;
        }

        public long id { get; }
        public BsonObjectId mongoId { get; set;  }
        public string FirstName { get; }
        public string LastName { get; }
        public string PhoneNum { get; }
        public string Email { get; }
        public Genders? Gender { get; }
    }


    internal static class OracleAccess
    {
        const string ConString = "User Id=COMPX323_02;Password=w3d8gu2SnT;Data Source=unioracle;";

        public static List<ArtistPreview> GetArtistContaining(string name)
        {
            OracleConnection con = new OracleConnection(ConString);

            OracleCommand cmd = con.CreateCommand();

            con.Open();

            cmd.CommandText = "SELECT\nID ,F_NAME, L_NAME\nFROM\nARTIST\nINNER JOIN PERSON ON ARTIST.PERSON_ID = PERSON.ID\n" +
                              "WHERE LOWER(F_NAME) LIKE '%' || :name || '%' OR LOWER(L_NAME) LIKE '%' || :name || '%'";
            cmd.Parameters.Add(new OracleParameter("name", OracleDbType.Varchar2) { Value = name.ToLower() });

            OracleDataReader reader = cmd.ExecuteReader();

            var out_val = new List<ArtistPreview>();

            while (reader.HasRows && reader.Read())
            {
                out_val.Add(new ArtistPreview(reader.GetInt64(0), reader.GetString(1), reader.GetString(2)));
            }

            reader.Close();
            con.Close();

            return out_val;
        }


        public static List<SongId> GetSongContaining(string title)
        {
            OracleConnection con = new OracleConnection(ConString);

            OracleCommand cmd = con.CreateCommand();

            con.Open();

            cmd.CommandText = "SELECT TITLE, ORIGINAL_RELEASE_DATE FROM SONG WHERE LOWER(TITLE) LIKE '%' || :title || '%'";
            cmd.Parameters.Add(new OracleParameter("title", OracleDbType.Varchar2) { Value = title.ToLower() });

            OracleDataReader reader = cmd.ExecuteReader();

            var out_val = new List<SongId>();

            while (reader.HasRows && reader.Read())
            {
                out_val.Add(new SongId(reader.GetString(0), reader.GetDateTime(1)));
            }

            reader.Close();
            con.Close();
            return out_val;
        }

        public static List<SongId> GetSongsBy(long artist)
        {
            OracleConnection con = new OracleConnection(ConString);

            OracleCommand cmd = con.CreateCommand();

            con.Open();

            cmd.CommandText = "SELECT SONG.TITLE, SONG.ORIGINAL_RELEASE_DATE FROM SONG JOIN WRITES \n" +
                              "\tON SONG.TITLE = WRITES.SONG_TITLE AND SONG.ORIGINAL_RELEASE_DATE = WRITES.SONG_ORIGINAL_RELEASE_DATE\n" +
                              "JOIN ARTIST \n" +
                              "\tON WRITES.ARTIST_ID = ARTIST.PERSON_ID\n" +
                              "WHERE ARTIST_ID = :artistID";
            cmd.Parameters.Add(new OracleParameter("artistID", OracleDbType.Int64) { Value = artist });

            OracleDataReader reader = cmd.ExecuteReader();

            var out_val = new List<SongId>();

            while (reader.HasRows && reader.Read())
            {
                out_val.Add(new SongId(reader.GetString(0), reader.GetDateTime(1)));
            }

            reader.Close();
            con.Close();
            return out_val;
        }

        public static Song? GetAllSongInfo(SongId id)
        {
            OracleConnection con = new OracleConnection(ConString);

            OracleCommand cmd = con.CreateCommand();

            con.Open();

            cmd.CommandText = "SELECT SONG.TITLE, SONG.ORIGINAL_RELEASE_DATE, SONG.LENGTH, ARTIST_ID, SONG.LYRICS FROM SONG LEFT OUTER JOIN WRITES \n" +
                              "\tON SONG.TITLE = WRITES.SONG_TITLE AND SONG.ORIGINAL_RELEASE_DATE = WRITES.SONG_ORIGINAL_RELEASE_DATE\n" +
                              " LEFT OUTER JOIN ARTIST \n\tON WRITES.ARTIST_ID = ARTIST.PERSON_ID\n" +
                              "WHERE SONG.TITLE = :title AND SONG.ORIGINAL_RELEASE_DATE = :ogrd";

            cmd.Parameters.Add(new OracleParameter("title", OracleDbType.Varchar2) { Value = id.Title });
            cmd.Parameters.Add(new OracleParameter("ogrd", OracleDbType.Date) { Value = id.ReleaseDate });
            cmd.InitialLONGFetchSize = -1;

            OracleDataReader reader = cmd.ExecuteReader();

            // TODO: Add album thing

            Song? out_val = null;
            if (reader.Read())
            {
                out_val = new Song(reader.GetString(0), reader.GetInt64(2), reader.GetDateTime(1), reader.GetString(4), new List<long>(), null);
                if (!reader.IsDBNull(3))
                {
                    out_val.Value.ArtistIDs.Add(reader.GetInt64(3));
                }
            }

            while (out_val.HasValue && reader.Read())
            {
                out_val.Value.ArtistIDs.Add(reader.GetInt64(3));
            }

            reader.Close();
            con.Close();
            return out_val;
        }

        public static Person? GetAllPersonInfo(long id)
        {
            OracleConnection con = new OracleConnection(ConString);

            OracleCommand cmd = con.CreateCommand();

            con.Open();

            cmd.CommandText = "SELECT\n" +
                              "COMPX323_02.PERSON.ID,\n" +
                              "COMPX323_02.PERSON.F_NAME,\n" +
                              "COMPX323_02.PERSON.L_NAME,\n" +
                              "COMPX323_02.PERSON.PHONE,\n" +
                              "COMPX323_02.PERSON.EMAIL,\n" +
                              "COMPX323_02.PERSON.GENDER \n" +
                              "    FROM\n" +
                              "COMPX323_02.PERSON \n" +
                              "    WHERE\n" +
                              "COMPX323_02.PERSON.ID = :id";

            cmd.Parameters.Add(new OracleParameter("id", OracleDbType.Int64) { Value = id });

            OracleDataReader reader = cmd.ExecuteReader();

            Person? out_val = null;
            if (reader.Read())
            {
                out_val = new Person(id,
                    reader.GetString(1),
                    reader.GetString(2),
                    reader.GetString(3),
                    reader.GetString(4),
                    (Person.Genders)Enum.Parse(typeof(Person.Genders), reader.GetString(5)));
            }

            reader.Close();
            con.Close();
            return out_val;
        }

        public static long? AddPerson(Person newPerson)
        {
            OracleConnection con = new OracleConnection(ConString);

            OracleCommand cmd = con.CreateCommand();

            con.Open();

            cmd.CommandText = "INSERT INTO PERSON (ID, F_NAME, L_NAME, EMAIL, PHONE, GENDER) VALUES\n" +
                              "(person_seq.NEXTVAL, :fname, :lname, :email, :phone, :gender)";

            cmd.Parameters.Add(new OracleParameter("fname", OracleDbType.Varchar2) { Value = newPerson.FirstName });
            cmd.Parameters.Add(new OracleParameter("lname", OracleDbType.Varchar2) { Value = newPerson.LastName });
            cmd.Parameters.Add(new OracleParameter("email", OracleDbType.Varchar2) { Value = newPerson.Email });
            cmd.Parameters.Add(new OracleParameter("phone", OracleDbType.Varchar2) { Value = newPerson.PhoneNum });
            cmd.Parameters.Add(new OracleParameter("gender", OracleDbType.Varchar2) { Value = newPerson.Gender.HasValue ? newPerson.Gender.ToString() : "" });

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (OracleException e)
            {
                con.Close();
                return null;
            }

            OracleCommand getLatestIDcmd = con.CreateCommand();

            getLatestIDcmd.CommandText = "SELECT PERSON_SEQ.currval FROM all_sequences WHERE sequence_name='PERSON_SEQ'";

            var reader = getLatestIDcmd.ExecuteReader();

            reader.Read();
            long out_val = reader.GetInt64(0);
            Debug.WriteLine(out_val);
            reader.Close();
            con.Close();

            return out_val;
        }

        public static bool SetPersonIsArtist(long personId, bool isArtist)
        {
            OracleConnection con = new OracleConnection(ConString);

            OracleCommand cmd = con.CreateCommand();

            con.Open();
            if (isArtist)
            {
                cmd.CommandText = "INSERT INTO ARTIST (PERSON_ID) VALUES (:id)";
                cmd.Parameters.Add(new OracleParameter("id", OracleDbType.Int64) { Value = personId });
            }
            else
            {
                cmd.CommandText = "DELETE FROM ARTIST WHERE PERSON_ID = :id";
                cmd.Parameters.Add(new OracleParameter("id", OracleDbType.Int64) { Value = personId });
            }

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (OracleException e) // This will trigger when INSERT or DELETE fails due to foreign constraint (not when they don't exist)
            {
                throw e;
                con.Close();
                return false;
            }

            con.Close();
            return true;
        }

        /// <summary>
        /// Adds a new song into the oracle database
        /// </summary>
        /// <param name="newSong">The new song to add</param>
        /// <returns>Returns true if operation completed successfully, returns false if not (database might be modified though)</returns>
        public static bool AddSong(Song newSong)
        {
            OracleConnection con = new OracleConnection(ConString);

            OracleCommand addSongCmd = con.CreateCommand();

            con.Open();

            addSongCmd.CommandText = "INSERT INTO SONG ( TITLE, ORIGINAL_RELEASE_DATE, LENGTH,  LYRICS)\n" +
                                     "VALUES (:title, :ogrd, :length, :lyrics)";

            addSongCmd.Parameters.Add(new OracleParameter("title", OracleDbType.Varchar2) { Value = newSong.Title });
            addSongCmd.Parameters.Add(new OracleParameter("ogrd", OracleDbType.Date) { Value = newSong.OriginalReleaseDate });
            addSongCmd.Parameters.Add(new OracleParameter("length", OracleDbType.Int64) { Value = newSong.Length });
            addSongCmd.Parameters.Add(new OracleParameter("lyrics", OracleDbType.Long) { Value = newSong.Lyrics });
            try
            {
                addSongCmd.ExecuteNonQuery();
            }
            catch (OracleException e)
            {
                return false;
            }

            string cmdText = "INSERT ALL\n";

            foreach (long artistID in newSong.ArtistIDs)
            {
                cmdText += "INTO WRITES (ARTIST_ID,SONG_TITLE, SONG_ORIGINAL_RELEASE_DATE) VALUES (" + artistID + ", :title, :ogrd)\n";
            }

            cmdText += "SELECT 1 FROM DUAL";

            OracleCommand addWritesCmd = con.CreateCommand();

            addWritesCmd.CommandText = cmdText;

            addWritesCmd.Parameters.Add(new OracleParameter("title", OracleDbType.Varchar2) { Value = newSong.Title });
            addWritesCmd.Parameters.Add(new OracleParameter("ogrd", OracleDbType.Date) { Value = newSong.OriginalReleaseDate });

            try
            {
                addWritesCmd.ExecuteNonQuery();
            }
            catch (OracleException e)
            {
                con.Close();
                return false;
            }
            con.Close();
            //rowsUpdated = addWritesCmd.ExecuteNonQuery();
            return true;
        }

        public static bool RemoveSong(SongId id)
        {
            OracleConnection con = new OracleConnection(ConString);

            OracleCommand removeSongCmd = con.CreateCommand();

            con.Open();

            removeSongCmd.CommandText = "DELETE FROM SONG WHERE\n" +
                         "SONG.TITLE = :title AND SONG.ORIGINAL_RELEASE_DATE = :ogrd";

            removeSongCmd.Parameters.Add(new OracleParameter("title", OracleDbType.Varchar2) { Value = id.Title });
            removeSongCmd.Parameters.Add(new OracleParameter("ogrd", OracleDbType.Date) { Value = id.ReleaseDate });

            try
            {
                removeSongCmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (OracleException e)
            {
                con.Close();
                return false;
            }
        }

        public static bool EditSong(Song editedSong)
        {
            OracleConnection con = new OracleConnection(ConString);

            OracleCommand editSongCmd = con.CreateCommand();

            con.Open();

            editSongCmd.CommandText = "UPDATE SONG SET LYRICS = :lyrics, LENGTH = :length\n" +
                         "WHERE SONG.TITLE = :title AND SONG.ORIGINAL_RELEASE_DATE = :ogrd";

            editSongCmd.Parameters.Add(new OracleParameter("lyrics", OracleDbType.Long) { Value = editedSong.Lyrics });
            editSongCmd.Parameters.Add(new OracleParameter("length", OracleDbType.Int64) { Value = editedSong.Length });
            editSongCmd.Parameters.Add(new OracleParameter("title", OracleDbType.Varchar2) { Value = editedSong.Title });
            editSongCmd.Parameters.Add(new OracleParameter("ogrd", OracleDbType.Date) { Value = editedSong.OriginalReleaseDate });

            try
            {
                editSongCmd.ExecuteNonQuery();

                OracleCommand removeWrites = con.CreateCommand();

                removeWrites.CommandText = "DELETE FROM WRITES WHERE WRITES.SONG_TITLE = :title AND WRITES.SONG_ORIGINAL_RELEASE_DATE = :ogrd";
                removeWrites.Parameters.Add(new OracleParameter("title", OracleDbType.Varchar2) { Value = editedSong.Title });
                removeWrites.Parameters.Add(new OracleParameter("ogrd", OracleDbType.Date) { Value = editedSong.OriginalReleaseDate });

                removeWrites.ExecuteNonQuery();

                string cmdText = "INSERT ALL\n";
                foreach (var id in editedSong.ArtistIDs)
                {
                    cmdText += "INTO WRITES (ARTIST_ID,SONG_TITLE, SONG_ORIGINAL_RELEASE_DATE) VALUES (" + id + ", :title, :ogrd)\n";
                }
                cmdText += "SELECT 1 FROM DUAL";
                OracleCommand addWrites = con.CreateCommand();
                addWrites.CommandText = cmdText;

                addWrites.Parameters.Add(new OracleParameter("title", OracleDbType.Varchar2) { Value = editedSong.Title });
                addWrites.Parameters.Add(new OracleParameter("ogrd", OracleDbType.Date) { Value = editedSong.OriginalReleaseDate });

                addWrites.ExecuteNonQuery();

                con.Close();
                return true;
            }
            catch (OracleException e)
            {
                con.Close();
                return false;
            }
        }
    }
}