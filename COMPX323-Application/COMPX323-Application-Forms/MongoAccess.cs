﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Diagnostics;

namespace COMPX323_Application_Forms
{
    public abstract class MongoDbSong
    {
        public ObjectId _id { set; get; }
        public BsonString title { get; set; }
        public BsonInt64 length { get; set; }
        public BsonDateTime original_release_date { get; set; }
        public BsonString lyrics { get; set; }
    }

    static class MongoAccess
    {
        const string CON_STRING = "mongodb://mongodb.cms.waikato.ac.nz:27017";
        static MongoClient client;
        public static void Connect()
        {
            string username = "compx323-02";
            string password = "m8t7WtuyFkzW3kWtN22M";
            string mongoDbAuthMechanism = "SCRAM-SHA-1";
            MongoInternalIdentity internalIdentity =
                      new MongoInternalIdentity("admin", username);
            PasswordEvidence passwordEvidence = new PasswordEvidence(password);
            MongoCredential mongoCredential =
                 new MongoCredential(mongoDbAuthMechanism,
                         internalIdentity, passwordEvidence);


            MongoClientSettings settings = new MongoClientSettings();
            // comment this line below if your mongo doesn't run on secured mode
            settings.Credential = mongoCredential;
            String mongoHost = "mongodb.cms.waikato.ac.nz";
            MongoServerAddress address = new MongoServerAddress(mongoHost);
            settings.Server = address;
            client = new MongoClient(settings);
        }

        public static List<SongId> GetSongContaining(string title)
        {

            var our_db = client.GetDatabase("compx323-02");
            var batch = our_db.GetCollection<BsonDocument>("SongSearcher").Find("{title: /" + title + "/i}").ToList();

            return batch.Select(c =>
            {
                return new SongId(id: c.GetElement("_id").Value.AsObjectId, title: c.GetElement("title").Value.AsString, releaseDate: c.GetElement("original_release_date").Value.ToUniversalTime());
            }).ToList();
            //foreach (BsonDocument document in batch)
            //{
            //    Console.WriteLine(document);
            //    Console.WriteLine();
            //}
            //return new List<SongId>();
        }

        public static List<ArtistPreview> GetArtistContaining(string name)
        {
            var our_db = client.GetDatabase("compx323-02");
            var filter = Builders<BsonDocument>.Filter;
            var batch = our_db.GetCollection<BsonDocument>("SongSearcher").Find(filter.Regex("first_name", new BsonRegularExpression(name, "i")) | filter.Regex("last_name", new BsonRegularExpression(name, "i"))).ToList();

            return batch.Select(c =>
            {
                return new ArtistPreview(id: c.GetElement("_id").Value.AsObjectId, firstName: c.GetElement("first_name").Value.AsString, lastName: c.GetElement("last_name").Value.AsString);
            }).ToList();
        }

        public static List<SongId> GetSongsBy(long artist)
        {
            return new List<SongId>();
        }

        public static Song? GetAllSongInfo(SongId id)
        {
            var our_db = client.GetDatabase("compx323-02");
            var filter = Builders<BsonDocument>.Filter;
            var collection = our_db.GetCollection<BsonDocument>("SongSearcher");
            var batch = collection.Find(filter.Eq("_id", id.Id)).FirstOrDefault();
            if (batch == null)
            {
                return null;
            }
            else
            {

                return new Song(
                    batch.GetElement("_id").Value.AsObjectId,
                    batch.GetElement("title").Value.AsString, 
                    batch.GetElement("length").Value.AsInt64, 
                    batch.GetElement("original_release_date").Value.ToUniversalTime(), 
                    batch.GetElement("lyrics").Value.AsString,
                    collection.Find(filter.In("writes", new[] { id.Id })).ToList().Select(c => new BsonObjectId(c.GetElement("_id").Value.AsObjectId)).ToList(), null);
            }


        }

        public static Person? GetAllPersonInfo(BsonObjectId id)
        {
            var our_db = client.GetDatabase("compx323-02");
            var filter = Builders<BsonDocument>.Filter;
            var batch = our_db.GetCollection<BsonDocument>("SongSearcher").Find(filter.Eq("_id", id)).FirstOrDefault();
            if (batch == null)
            {
                return null;
            } else
            {
                return new Person(id, batch.GetElement("first_name").Value.AsString, batch.GetElement("last_name").Value.AsString, "", "", null);
            }
        }

        public static bool AddPerson(Person newPerson)
        {
            var our_db = client.GetDatabase("compx323-02");
            var collection = our_db.GetCollection<BsonDocument>("SongSearcher");
            if (newPerson.mongoId == null)
            {
                newPerson.mongoId = new BsonObjectId(ObjectId.GenerateNewId());
            }

            collection.InsertOne(new BsonDocument
            {
                { "_id",  newPerson.mongoId},
                {"first_name", BsonValue.Create(newPerson.FirstName) },
                {"last_name", BsonValue.Create(newPerson.LastName) },
                {"email", newPerson.Email == "" ? null : BsonValue.Create(newPerson.Email) },
                {"ph_num", newPerson.PhoneNum == "" ? null : BsonValue.Create(newPerson.PhoneNum) },
            });

            return true;
        }

        public static bool SetPersonIsArtist(long personId, bool isArtist)
        {
            return true;
        }

        public static bool AddSong(Song newSong)
        {
            var our_db = client.GetDatabase("compx323-02");
            var collection = our_db.GetCollection<BsonDocument>("SongSearcher");
            if (newSong.Id == null)
            {
                newSong.Id = new BsonObjectId(ObjectId.GenerateNewId());
            }

            collection.InsertOne(new BsonDocument
            {
                { "_id",  newSong.Id},
                {"title", BsonValue.Create(newSong.Title) },
                {"length", BsonValue.Create(newSong.Length) },
                {"original_release_date", BsonValue.Create(newSong.OriginalReleaseDate) },
                {"lyrics", BsonValue.Create(newSong.Lyrics) },
            });
            var filter = Builders<BsonDocument>.Filter;
            var update = Builders<BsonDocument>.Update;
            foreach (var art in newSong.ArtistIDsMongo)
            {
                collection.UpdateOne(filter.Eq("_id", art), update.Push("writes", newSong.Id));
            }
            return true;
        }

        public static bool RemoveSong(SongId id)
        {
            var update = Builders<BsonDocument>.Update;
            var filter = Builders<BsonDocument>.Filter;
            var our_db = client.GetDatabase("compx323-02");
            var collection = our_db.GetCollection<BsonDocument>("SongSearcher");
            collection.UpdateMany(filter.In("writes", new[] { id.Id }), update.Pull("writes", id.Id));
            var res = collection.DeleteOne(filter.Eq("_id", id.Id));
            return res.DeletedCount > 0;

        }
        public static bool EditSong(Song editedSong)
        {
            var filter = Builders<BsonDocument>.Filter;
            var update = Builders<BsonDocument>.Update;
            var our_db = client.GetDatabase("compx323-02");
            var collection = our_db.GetCollection<BsonDocument>("SongSearcher");
            collection.UpdateMany(filter.In("writes", new[] { editedSong.Id }), update.Pull("writes", editedSong.Id));

            collection.UpdateOne(filter.Eq("_id", editedSong.Id), update
                .Set("title", editedSong.Title)
                .Set("length", editedSong.Length)
                .Set("original_release_date", editedSong.OriginalReleaseDate)
                .Set("lyrics", editedSong.Lyrics));

            foreach (var art in editedSong.ArtistIDsMongo)
            {
                collection.UpdateOne(filter.Eq("_id", art), update.Push("writes", editedSong.Id));
            }

            return true;
        }
    }
}
