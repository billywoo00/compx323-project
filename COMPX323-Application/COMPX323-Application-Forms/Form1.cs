﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Linq;
using MongoDB.Bson;

namespace COMPX323_Application_Forms
{
    public partial class Form1 : Form
    {
        Boolean search_Song = true;
        Boolean search_Artist = false;
        Boolean song_Found = false;
        Boolean artist_Found = false;
        Boolean is_oracle = true;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            resultBox.Items.Clear();
            //detailBox.Items.Clear();
            string song = "";
            string artist = "";

            if (textBoxValue.Text.Length < 3)
            {
                MessageBox.Show("Please enter at least 3 characters");
                textBoxValue.Focus();
                return;
            }
            //try
            {
                //If user is searching for a song
                if (search_Song == true && search_Artist == false)
                {
                    song = textBoxValue.Text;
                    List<SongId> dataS;

                    if (is_oracle == true)
                    {
                        dataS = OracleAccess.GetSongContaining(song);
                    }
                    else
                    {
                        dataS = MongoAccess.GetSongContaining(song);
                    }


                    List<String> title = new List<String>();
                    List<DateTime> RD = new List<DateTime>();

                    //If list is empty
                    if (dataS.Capacity == 0)
                    {
                        //No song found
                        song_Found = false;
                    }
                    else
                    {
                        song_Found = true;
                    }
                    //If song was found
                    if (song_Found == true)
                    {
                        initialiseTextBoxes();
                        //For each song found
                        foreach (SongId name in dataS)
                        {
                            //Add it into the listbox
                            resultBox.Items.Add(name);
                            //   detailBox.Items.Add(name.ReleaseDate);
                        }
                        //For each data
                        foreach (SongId name in dataS)
                        {
                            //Store the title of song and releaseDate
                            title.Add(name.Title);
                            RD.Add(name.ReleaseDate);
                        }
                    }

                }
                //Else if user is searching for an artist
                else if (search_Artist == true && search_Song == false)
                {
                    artist = textBoxValue.Text;

                    List<ArtistPreview> dataA;

                    if (is_oracle == true)
                    {
                        dataA = OracleAccess.GetArtistContaining(artist);
                    }
                    else
                    {
                        dataA = MongoAccess.GetArtistContaining(artist);
                    }


                    List<String> fname = new List<String>();
                    List<String> lname = new List<String>();
                    List<long> Id = new List<long>();

                    //If list is empty
                    if (dataA.Capacity == 0)
                    {
                        //No artist Found
                        artist_Found = false;
                    }
                    else
                    {
                        artist_Found = true;
                    }

                    if (artist_Found == true)
                    {
                        initialiseTextBoxes();
                        //For each artist name found
                        foreach (ArtistPreview name in dataA)
                        {
                            //Add it into the listbox
                            resultBox.Items.Add(name.FirstName);
                        }
                        //For each data
                        foreach (ArtistPreview name in dataA)
                        {
                            //Store in first name, last name and id
                            fname.Add(name.FirstName);
                            lname.Add(name.LastName);
                            Id.Add(name.Id);

                        }
                    }
                }

            }
            //catch
            //{
            //    MessageBox.Show("This Song/Artist name isn't valid");
            //    textBoxValue.Clear();
            //    textBoxValue.Focus();
            //}
        }

        public void resultBoxValue_Click()
        {
            string songn = resultBox.GetItemText(resultBox.SelectedItem);
            // OracleAccess.GetAllSongInfo(songn);
        }

        public void initialiseTextBoxes()
        {
            //clears all text boxes
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    (c as TextBox).Clear();
                }
            }
            //set focus to username textbox.
            textBoxValue.Focus();
        }
        /// <summary>
        /// To check if users searching for artist
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void artistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            search_Artist = true;
            search_Song = false;
            label1.Text = "Artist Name : ";
            resultBox.Items.Clear();
            // detailBox.Items.Clear();
        }
        /// <summary>
        /// To see if users searching for Song
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void songToolStripMenuItem_Click(object sender, EventArgs e)
        {
            search_Song = true;
            search_Artist = false;
            label1.Text = "Song Name : ";
            resultBox.Items.Clear();
            //     detailBox.Items.Clear();
        }
        /// <summary>
        /// If user is trying to add a song to dictionary
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addSongArtistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddSong addpopup = new AddSong(is_oracle);
            addpopup.ShowDialog();
        }
        Song currentEditingSong = new Song();

        bool hasBeenEdited = false;

        enum SongValType
        {
            Title,
            Lyrics,
            Length,
            Date,
        }

        private void resultBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (search_Song && !search_Artist)
            {
                detailsPanel.Controls.Clear();
                if (resultBox.SelectedItem == null)
                {
                    return;
                }
                SongId selected = (SongId)resultBox.SelectedItem;
                Song? all_song_info;
                if (is_oracle == true)
                {
                    all_song_info = OracleAccess.GetAllSongInfo(selected);
                }
                else
                {
                    all_song_info = MongoAccess.GetAllSongInfo(selected);
                }

                currentEditingSong = all_song_info.Value;
                hasBeenEdited = false;
                saveBtn.Enabled = false;
                revertBtn.Enabled = false;
                if (all_song_info.HasValue)
                {
                    List<Person> personInfo;
                    if (is_oracle == true)
                    {
                        personInfo = all_song_info.Value.ArtistIDs.Select(c => OracleAccess.GetAllPersonInfo(c).Value).ToList();
                    }
                    else
                    {
                        personInfo = all_song_info.Value.ArtistIDsMongo.Select(c => MongoAccess.GetAllPersonInfo(c).Value).ToList();
                    }

                    var songTitleLabel = labelTextbox("Song Title: ", true);
                    detailsPanel.Controls.Add(songTitleLabel, 0, 0);
                    var songTitleVal = labelTextbox(all_song_info.Value.Title, true);
                    songTitleVal.Tag = SongValType.Title;
                    songTitleVal.TextChanged += detailValueChanged;
                    detailsPanel.Controls.Add(songTitleVal, 1, 0);
                    var songLengthLabel = labelTextbox("Song Length: ", true);
                    detailsPanel.Controls.Add(songLengthLabel, 0, 1);
                    var songLengthVal = labelTextbox(all_song_info.Value.Length.ToString(), false);
                    songLengthVal.TextChanged += detailValueChanged;
                    songLengthVal.Tag = SongValType.Length;
                    detailsPanel.Controls.Add(songLengthVal, 1, 1);
                    var songDateLabel = labelTextbox("Release Date: ", true);
                    detailsPanel.Controls.Add(songDateLabel, 0, 2);
                    var songDateVal = new DateTimePicker();
                    songDateVal.Value = all_song_info.Value.OriginalReleaseDate;
                    songDateVal.ValueChanged += detailValueChanged;
                    songDateVal.Dock = DockStyle.Fill;
                    songDateVal.Tag = SongValType.Date;
                    songDateVal.Enabled = false;
                    detailsPanel.Controls.Add(songDateVal, 1, 2);
                    var songLyricsLabel = labelTextbox("Lyrics: ", true);
                    detailsPanel.Controls.Add(songLyricsLabel, 0, 3);
                    var songLyricVal = labelTextbox(all_song_info.Value.Lyrics, false);
                    songLyricVal.TextChanged += detailValueChanged;
                    songLyricVal.Tag = SongValType.Lyrics;
                    detailsPanel.Controls.Add(songLyricVal, 1, 3);

                    var artLabel = labelTextbox("Artists: ", true);
                    detailsPanel.Controls.Add(artLabel, 0, 4);
                    for (int i = 0; i < personInfo.Count; i++)
                    {
                        var currentVal = labelTextbox(personInfo[i].FirstName + " " + personInfo[i].LastName, true);
                        currentVal.Click += artistNameClick;
                        if (is_oracle)
                        {
                            currentVal.Tag = personInfo[i].id;

                        }
                        else
                        {
                            currentVal.Tag = personInfo[i].mongoId;

                        }
                        detailsPanel.Controls.Add(currentVal, 1, 4 + i);
                    }

                    var addNewArtistToSongBtn = new Button();
                    addNewArtistToSongBtn.Text = "Add new artist";
                    addNewArtistToSongBtn.Click += AddNewArtistToSongBtn_Click;
                    addNewArtistToSongBtn.Dock = DockStyle.Fill;
                    detailsPanel.Controls.Add(addNewArtistToSongBtn, 1, 4 + personInfo.Count);
                    detailsPanel.SetRowSpan(addNewArtistToSongBtn, 2);
                }
            }
            else if (search_Artist && !search_Song)
            {

            }
        }

        private void artistNameClick(object sender, EventArgs e)
        {
            if (is_oracle == true)
            {
                currentEditingSong.ArtistIDs.Remove((long)((Control)sender).Tag);
                ((TextBox)sender).Parent.Controls.Remove((TextBox)sender);
            }
            else
            {
                currentEditingSong.ArtistIDsMongo.Remove((BsonObjectId)((Control)sender).Tag);
                ((TextBox)sender).Parent.Controls.Remove((TextBox)sender);
            }

            hasBeenEdited = true;
            saveBtn.Enabled = true;
            revertBtn.Enabled = true;
        }

        private void AddNewArtistToSongBtn_Click(object sender, EventArgs e)
        {
            AddArtistToSong art = new AddArtistToSong(is_oracle);
            var res = art.ShowDialog();
            if (res == DialogResult.OK)
            {
                //Person? personInfo;
                //if (is_oracle == true)
                //{
                //    personInfo = OracleAccess.GetAllPersonInfo(art.ReturnVal);
                //}
                //else
                //{
                //    personInfo = MongoAccess.GetAllPersonInfo(art.ReturnVal);
                //}

                //if (personInfo.HasValue)
                //{
                var currentVal = labelTextbox(art.ReturnVal.FirstName + " " + art.ReturnVal.LastName, true);
                currentVal.Click += artistNameClick;
                currentVal.Tag = art.ReturnVal;

                if (is_oracle)
                {
                    detailsPanel.SetRow(detailsPanel.GetControlFromPosition(1, 4 + currentEditingSong.ArtistIDs.Count), 4 + currentEditingSong.ArtistIDs.Count + 1);
                    detailsPanel.Controls.Add(currentVal, 1, 4 + currentEditingSong.ArtistIDs.Count);
                    currentEditingSong.ArtistIDs.Add(art.ReturnVal.Id);
                }
                else
                {
                    detailsPanel.SetRow(detailsPanel.GetControlFromPosition(1, 4 + currentEditingSong.ArtistIDsMongo.Count), 4 + currentEditingSong.ArtistIDsMongo.Count + 1);
                    detailsPanel.Controls.Add(currentVal, 1, 4 + currentEditingSong.ArtistIDsMongo.Count);
                    currentEditingSong.ArtistIDsMongo.Add(art.ReturnVal.MongoId);
                }
                hasBeenEdited = true;
                saveBtn.Enabled = true;
                revertBtn.Enabled = true;
                //}

            }
            else if (res == DialogResult.Cancel)
            {
                return;
            }
        }

        private void detailValueChanged(object sender, EventArgs e)
        {
            hasBeenEdited = true;
            saveBtn.Enabled = true;
            revertBtn.Enabled = true;
            switch ((SongValType)((Control)sender).Tag)
            {
                //case SongValType.Title:
                //    currentEditingSong.Title = ((TextBox)sender).Text;
                //    break;
                case SongValType.Lyrics:
                    currentEditingSong.Lyrics = ((TextBox)sender).Text;
                    break;
                case SongValType.Length:
                    currentEditingSong.Length = int.Parse(((TextBox)sender).Text);
                    break;
                    //case SongValType.Date:
                    //    currentEditingSong.OriginalReleaseDate = ((DateTimePicker)sender).Value;
                    //    break;
            }

        }

        private TextBox labelTextbox(string text, bool read_only)
        {
            var songTitleLabel = new TextBox();
            songTitleLabel.Text = text;
            songTitleLabel.ReadOnly = read_only;
            songTitleLabel.Dock = DockStyle.Fill;
            return songTitleLabel;
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (is_oracle == true)
            {
                if (OracleAccess.EditSong(currentEditingSong))
                {
                    MessageBox.Show("Saved");
                }
                else
                {
                    MessageBox.Show("Saving Failed");
                }
            }
            else
            {
                if (MongoAccess.EditSong(currentEditingSong))
                {
                    MessageBox.Show("Saved");
                }
                else
                {
                    MessageBox.Show("Saving Failed");
                }
            }

        }

        private void revertBtn_Click(object sender, EventArgs e)
        {
            resultBox_SelectedValueChanged(detailsPanel, new EventArgs());
        }

        private void delete_selected_btn_Click(object sender, EventArgs e)
        {
            if (is_oracle == true)
            {
                OracleAccess.RemoveSong((SongId)resultBox.SelectedItem);
                resultBox.Items.RemoveAt(resultBox.SelectedIndex);
            }
            else
            {
                MongoAccess.RemoveSong((SongId)resultBox.SelectedItem);
                resultBox.Items.RemoveAt(resultBox.SelectedIndex);
            }

        }

        private void switchOracleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            is_oracle = true;
            switchOracleToolStripMenuItem.Checked = true;
            swtichToMongoToolStripMenuItem.Checked = false;
            resultBox.Items.Clear();
            detailsPanel.Controls.Clear();
        }

        private void swtichToMongoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            is_oracle = false;
            switchOracleToolStripMenuItem.Checked = false;
            swtichToMongoToolStripMenuItem.Checked = true;
            resultBox.Items.Clear();
            detailsPanel.Controls.Clear();
        }

        private void addArtistToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddArtist addpopup = new AddArtist(is_oracle);
            addpopup.ShowDialog();
        }
    }
}
