﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Diagnostics;


namespace COMPX323_Application_Forms
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //            List<SongId> data = OracleAccess.GetSongContaining("cool");
            //            Song? allSongInfo = OracleAccess.GetAllSongInfo(data[0]);
            //            bool output = OracleAccess.AddSong(new Song("WEEEEE", 444, new DateTime(4353543534), "yadagergrggfe", new List<long> {6}, 5));
            //            OracleAccess.EditSong(new Song("Come Together", 2555, new DateTime(1939, 1, 2), "EWFEFE", new List<long>(), null));
            //            OracleAccess.SetPersonIsArtist(555, true);

            MongoAccess.Connect();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}