function generateText(length) {
  let result = "";
  let possible = "abcdefghijklmnopqrstuvwxyz";

  for (let i = 0; i <= length; i++)
    result += possible.charAt(Math.floor(Math.random() * (possible.length)));

  return result;
}

function generateGender() {
  var possible = ["male", "female", "other"];
  var randid = Math.floor(Math.random() * 3);
  return possible[randid];
}

for (let i = 0; i < 1; i++) {

  //Album Data
  let albumName = "'" + generateText(Math.floor(Math.random() * 7) + 10) + "'";
  let albumImage = "'" + generateText(Math.floor(Math.random() * 7) + 3) + ".png'";
  //Person Data
  let person_fname = "'" + generateText(Math.floor(Math.random() * 7) + 5) + "'";
  let person_lname = "'" + generateText(Math.floor(Math.random() * 7) + 5)+ "'";
  let person_phone_num = "'" + ("" + Math.floor(Math.random() * 100)) + "-" + Math.floor(Math.random() * 100000000)+ "'";
  let person_email = "'" + generateText(Math.floor(Math.random() * 7) + 3) + "@" + generateText(Math.floor(Math.random() * 7) + 3) + "." + generateText(2)+ "'";
  let person_gender = "'" + generateGender()+ "'";


  console.log(`INSERT INTO ALBUM values (album_seq.NEXTVAL, ${albumName}, ${albumImage});`)
  console.log(`INSERT INTO PERSON values (person_seq.NEXTVAL, ${person_fname}, ${person_lname}, ${person_phone_num}, ${person_email}, ${person_gender});`)
  
  console.log(`INSERT INTO ARTIST values(person_seq.CURRVAL);`)
  
  let songs_per_album =  Math.floor(Math.random() * 7) + 5;;
  for (let i2 = 0; i2 < 100000; i2++) {
	    //SongTitle
  let song_title = "'" + generateText(Math.floor(Math.random() * 7) + 20).trim() + "'";
  let song_length = "'" + Math.floor(Math.random() * 200 + 20) + "'";
  let song_original_release_date = "date '" + Math.floor(Math.random() * 200 + 1819) + "-" + Math.floor(Math.random() * 12 + 1) + "-" + Math.floor(Math.random() * 12 + 1) + "'";
  let song_lyrics = "'" + generateText(Math.floor(Math.random() * 10) + 20) + "'";
  
  console.log(`INSERT INTO SONG values (${song_title}, ${song_length}, ${song_original_release_date}, ${song_lyrics});`)
  }


}
